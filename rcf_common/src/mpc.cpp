/*******************************************************************************
 * BSD 3-Clause License
 *
 * Copyright (c) 2023, Zhou Qi
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

#include "rcf_common/mpc.h"

bool Mpc::init(const Eigen::MatrixXd& A, const Eigen::MatrixXd& B, const Eigen::MatrixXd& Q, const Eigen::MatrixXd& R,
               const Eigen::MatrixXd& qp_constraint, const int& horizon) {
  // get horizon
  horizon_ = horizon;
  // get M and C
  M_.resize(A.rows() * horizon_, A.cols());
  C_.resize(A.rows() * horizon_, B.cols() * horizon_);
  getMCMatrix(A, B);
  // Q and R
  Q_.resize(Q.rows() * horizon_, Q.cols() * horizon_);
  R_.resize(R.rows() * horizon_, R.cols() * horizon_);
  for (int i = 0; i < horizon_; i++) {
    Q_.block(Q.rows() * i, Q.cols() * i, Q.rows(), Q.cols()) = Q;
    R_.block(R.rows() * i, R.cols() * i, R.rows(), R.cols()) = R;
  }
  // check Q and R
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> > q_solver(Q_);
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> > r_solver(R_);
  if (q_solver.info() != Eigen::Success || r_solver.info() != Eigen::Success) return false;
  // q (r) must be symmetric positive (semi) definite
  for (int i = 0; i < q_solver.eigenvalues().cols(); ++i) {
    if (q_solver.eigenvalues()[i] < 0) return false;
  }
  for (int i = 0; i < r_solver.eigenvalues().cols(); ++i) {
    if (r_solver.eigenvalues()[i] <= 0) return false;
  }
  if (!isSymmetric(Q_) || !isSymmetric(R_)) return false;
  // H and G
  H_.resize(B.rows() * horizon_, B.cols() * horizon_);
  G_.resize(B.cols() * horizon_, 1);
  // u
  u_.resize(B.cols());
  // constraint
  qp_ub_.resize(qp_constraint.rows() * horizon_);
  qp_lb_.resize(qp_constraint.rows() * horizon_);
  qp_A_.resize(qp_constraint.rows() * horizon_, qp_constraint.rows() * horizon_);
  qp_A_.setIdentity();
  qp_ub_ = qp_constraint.replicate(horizon_, 1);
  qp_lb_ = -qp_constraint.replicate(horizon_, 1);
  // qp solver
  qp_solver_ = std::make_unique<qpOASES::SQProblem>(B.cols() * horizon_, qp_constraint.rows() * horizon_);
  qp_soln_ = new qpOASES::real_t();
  return true;
}

void Mpc::getMCMatrix(const Eigen::MatrixXd& A, const Eigen::MatrixXd& B) {
  // linear discretization
  Eigen::MatrixXd I;
  I.resize(A.rows(), A.cols());
  I.setIdentity();
  Eigen::MatrixXd lA = A * (1.0 / horizon_) + I;
  Eigen::MatrixXd lB = B * (1.0 / horizon_);
  // compute M and C
  M_.block(0, 0, A.rows(), A.cols()) = lA;
  C_.block(0, 0, B.rows(), B.cols()) = lB;
  for (int i = 1; i < horizon_; i++) {
    M_.block(A.rows() * i, 0, A.rows(), A.cols()) = lA * M_.block(A.rows() * (i - 1), 0, A.rows(), A.cols());
    C_.block(B.rows() * i, 0, B.rows(), B.cols() * i) = lA * C_.block(B.rows() * (i - 1), 0, B.rows(), B.cols() * i);
    C_.block(B.rows() * i, B.cols() * i, B.rows(), B.cols()) = lB;
  }
}

bool Mpc::setStatus(const Eigen::MatrixXd& x, const Eigen::MatrixXd& x_des) {
  Eigen::MatrixXd xs_des = x_des.replicate(horizon_, 1);
  // compute
  qpSolver(x, xs_des);
  return true;
}

void Mpc::qpSolver(const Eigen::MatrixXd& x_in, const Eigen::MatrixXd& xs_des) {
  qpOASES::Options op;
  op.setToMPC();
  op.printLevel = qpOASES::PL_LOW;
  qp_solver_->setOptions(op);
  qpOASES::returnValue status_sloving;
  int nWSR = 1000;
  // compute H and G
  H_ = 2 * C_.transpose() * Q_ * C_ + R_;               // 1/2 x.transpose() * H * x
  G_ = 2 * C_.transpose() * Q_ * (M_ * x_in - xs_des);  // x.transpose() * G
  if (first_solver_qp_) {
    status_sloving =
        qp_solver_->init(H_.data(), G_.data(), qp_A_.data(), nullptr, nullptr, qp_lb_.data(), qp_ub_.data(), nWSR);
    if (status_sloving > 0) {  // 无解
      qp_solver_->reset();
    } else {
      first_solver_qp_ = false;
    }
  } else {
    status_sloving =
        qp_solver_->hotstart(H_.data(), G_.data(), qp_A_.data(), nullptr, nullptr, qp_lb_.data(), qp_ub_.data(), nWSR);
    if (status_sloving > 0) {
      qp_solver_->reset();
      first_solver_qp_ = true;
    }
  }
  qp_solver_->getPrimalSolution(qp_soln_);
  if (status_sloving > 0) {
    u_ << 0, 0;
  } else {
    u_ << qp_soln_[0], qp_soln_[1];
  }
}

Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> Mpc::getControl() { return u_; }
