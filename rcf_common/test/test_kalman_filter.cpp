#include <nav_msgs/Path.h>
#include <ros/ros.h>

#include <chrono>
#include <random>

#include "rcf_common/filters/kalman_filter.h"

#define SAMPLE_RATE 1000
#define V_NOISE_MAG 0.08
#define W_NOISE_MAG 0.01

int main(int argc, char** argv) {
  // Set up filter
  Mat2<double> A, B, H, Q, R;

  A << 1., 0., 0., 1.;
  B << 1.0 / SAMPLE_RATE, 0, 0, 1.0 / SAMPLE_RATE;
  H << 1., 0., 0., 1.;
  Q << 300., 0., 0., 300.;
  R << 10000., 0., 0., 10000.;

  KalmanFilter<double> filter(A, B, H, Q, R);

  // 设置过程噪声与观测噪声采样器，均值为零的正态分布噪声，也称高斯白噪声
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine w_noise_gen(seed);
  std::default_random_engine v_noise_gen(seed + 1);
  std::normal_distribution<double> w_noise_dis(0.0, W_NOISE_MAG);
  std::normal_distribution<double> v_noise_dis(0.0, V_NOISE_MAG);
  // Set up ros
  ros::init(argc, argv, "kalman_test");
  ros::NodeHandle nh;
  ros::Publisher pub_real, pub_observe, pub_estimate;
  pub_real = nh.advertise<nav_msgs::Path>("real_traj", 100);
  pub_observe = nh.advertise<nav_msgs::Path>("observe_traj", 100);
  pub_estimate = nh.advertise<nav_msgs::Path>("estimate_traj", 100);
  nav_msgs::Path real_traj, observe_traj, estimate_traj;
  ros::Rate loop_rate(SAMPLE_RATE);

  double t = 0.;

  Vec2<double> x_real, filter_x_init, x_est, u, z, noise_w, noise_v;
  x_real << 1., 1.;
  filter_x_init << 0., 1.;
  u << 0., 0.;
  filter.clear(filter_x_init);
  real_traj.header.frame_id = "map";
  observe_traj.header.frame_id = "map";
  estimate_traj.header.frame_id = "map";
  while (ros::ok() && t < 2.) {
    // 控制量使得轨迹为正弦波
    u[0] = 5.0;
    u[1] = u[0] * sin(2 * u[0] * t);

    // 真实状态
    noise_w[0] = w_noise_dis(w_noise_gen);
    noise_w[1] = w_noise_dis(w_noise_gen);
    x_real = A * x_real + B * u + noise_w;
    geometry_msgs::PoseStamped real_pose;
    real_pose.pose.orientation.w = 1;
    real_pose.pose.position.x = x_real[0];
    real_pose.pose.position.y = x_real[1];
    real_traj.poses.push_back(real_pose);

    // 观测值
    noise_v[0] = v_noise_dis(v_noise_gen);
    noise_v[1] = v_noise_dis(v_noise_gen);
    z = x_real + noise_v;
    geometry_msgs::PoseStamped z_pose;
    z_pose.pose.orientation.w = 1;
    z_pose.pose.position.x = z[0];
    z_pose.pose.position.y = z[1];
    observe_traj.poses.push_back(z_pose);

    // 卡尔曼估计值
    filter.predict(u);
    filter.update(z);
    x_est = filter.getState();
    geometry_msgs::PoseStamped est_pose;
    est_pose.pose.orientation.w = 1;
    est_pose.pose.position.x = x_est[0];
    est_pose.pose.position.y = x_est[1];
    estimate_traj.poses.push_back(est_pose);

    t += 1. / (double)SAMPLE_RATE;
    loop_rate.sleep();
  }

  ros::Rate loop_rate2(1);
  while (ros::ok()) {
    pub_real.publish(real_traj);
    pub_observe.publish(observe_traj);
    pub_estimate.publish(estimate_traj);
    loop_rate2.sleep();
  }
  return 0;
}
