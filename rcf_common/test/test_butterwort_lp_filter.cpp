/*******************************************************************************
 * Copyright (c) 2023/05/03, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <cmath>

#include "rcf_common/filters/butterworth_lp_filter.h"
#include "rcf_msgs/LPTest.h"
int main(int argc, char** argv) {
  const double pi = 3.14159265358979323846;
  const double freq = 0.1;     // 正弦波的频率
  const double amplitude = 1;  // 正弦波的振幅

  // Set up ros
  ros::init(argc, argv, "butterworth_lp_test");
  ros::NodeHandle nh;
  ros::Publisher pub;
  pub = nh.advertise<rcf_msgs::LPTest>("lp_test_data", 100);
  rcf_msgs::LPTest lp_test_data;
  ButterworthLowPassFilter bw_lp_filter(3);
  bw_lp_filter.reset();

  ros::Rate loop_rate(1000);
  int pulse = 0;
  while (ros::ok()) {
    auto now_time = ros::Time::now();
    lp_test_data.header.stamp = now_time;

    double signal;
    // 生成信号
    signal = amplitude * std::sin(2 * pi * freq * now_time.toSec());
    lp_test_data.real = signal;

    // 将噪声添加到信号中
    double noise;
    if (pulse % 2000 == 0) {
      noise = 1.0;  // 观测器异常，出现突变值
    } else {
      noise = 0.1 * amplitude * static_cast<double>(rand()) / RAND_MAX;  // 高斯白噪声
    }

    signal += noise;
    lp_test_data.observe = signal;

    // 低通滤波
    bw_lp_filter.input(signal, now_time);
    signal = bw_lp_filter.output();
    lp_test_data.filter = signal;

    pub.publish(lp_test_data);
    pulse++;
    loop_rate.sleep();
    ros::spinOnce();
  }

  return 0;
}