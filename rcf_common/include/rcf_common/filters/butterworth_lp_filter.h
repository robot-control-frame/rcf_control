/*******************************************************************************
 * Copyright (c) 2023/05/03, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#pragma once

#include <dynamic_reconfigure/server.h>
#include <rcf_msgs/LpData.h>
#include <realtime_tools/realtime_publisher.h>

// My filter reference was Julius O. Smith III, Intro. to Digital Filters
// With Audio Applications.
// See https://ccrma.stanford.edu/~jos/filters/Example_Second_Order_Butterworth_Lowpass.html

class ButterworthLowPassFilter {
 public:
  explicit ButterworthLowPassFilter(ros::NodeHandle& nh) {
    nh.param("lp_cutoff_frequency", cutoff_frequency_, -1.);
    nh.param("lp_debug", is_debug_, false);

    if (is_debug_) realtime_pub_.reset(new realtime_tools::RealtimePublisher<rcf_msgs::LpData>(nh, "lp_filter", 100));
  };
  explicit ButterworthLowPassFilter(double cutoff_freq) {
    is_debug_ = false;
    cutoff_frequency_ = cutoff_freq;
  };

  void input(double in) { input(in, ros::Time::now()); }
  void input(double in, ros::Time time) {
    in_[2] = in_[1];
    in_[1] = in_[0];
    in_[0] = in;

    if (!prev_time_.isZero())  // Not first time through the program
    {
      delta_t_ = time - prev_time_;
      prev_time_ = time;
      if (0 == delta_t_.toSec()) {
        ROS_ERROR(
            "delta_t is 0, skipping this loop. Possible overloaded cpu "
            "at time: %f",
            time.toSec());
        return;
      }
    } else {
      prev_time_ = time;
      return;
    }

    if (cutoff_frequency_ != -1 && cutoff_frequency_ > 0) {
      // Check if tan(_) is tiny, could cause c = NaN
      tan_filt_ = tan((cutoff_frequency_ * 6.2832) * delta_t_.toSec() / 2.);
      // Avoid tan(0) ==> NaN
      if ((tan_filt_ <= 0.) && (tan_filt_ > -0.01)) tan_filt_ = -0.01;
      if ((tan_filt_ >= 0.) && (tan_filt_ < 0.01)) tan_filt_ = 0.01;
      c_ = 1 / tan_filt_;
    }

    out_[2] = out_[1];
    out_[1] = out_[0];
    out_[0] = (1 / (1 + c_ * c_ + M_SQRT2 * c_)) *
              (in_[2] + 2 * in_[1] + in_[0] - (c_ * c_ - M_SQRT2 * c_ + 1) * out_[2] - (-2 * c_ * c_ + 2) * out_[1]);

    if (is_debug_) {
      if (realtime_pub_->trylock()) {
        realtime_pub_->msg_.header.stamp = time;
        realtime_pub_->msg_.real = in_[0];
        realtime_pub_->msg_.filtered = out_[0];
        realtime_pub_->unlockAndPublish();
      }
    }
  }

  double output() { return out_[0]; }

  void reset() {
    for (int i = 0; i < 3; ++i) {
      in_[i] = 0;
      out_[i] = 0;
    }
  }

 private:
  double in_[3]{};
  double out_[3]{};

  // Cutoff frequency for the derivative calculation in Hz.
  // Negative -> Has not been set by the user yet, so use a default.
  double cutoff_frequency_ = -1;
  // Used in filter calculations. Default 1.0 corresponds to a cutoff frequency
  // at 1/4 of the sample rate.
  double c_ = 1.;
  // Used to check for tan(0)==>NaN in the filter calculation
  double tan_filt_ = 1.;
  bool is_debug_{};

  ros::Time prev_time_;
  ros::Duration delta_t_;

  std::shared_ptr<realtime_tools::RealtimePublisher<rcf_msgs::LpData>> realtime_pub_{};
};
