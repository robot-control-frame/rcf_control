/*******************************************************************************
 * Copyright (c) 2023, Zhou Qi
 * All rights reserved.
 *******************************************************************************/

#pragma once

#include <Eigen/Dense>
#include <iostream>
#include <memory>

#include "eigen_types.h"
#include "qpOASES.hpp"

class Mpc {
 public:
  Mpc() = default;
  ~Mpc() = default;

  bool init(const Eigen::MatrixXd& A, const Eigen::MatrixXd& B, const Eigen::MatrixXd& Q, const Eigen::MatrixXd& R,
            const Eigen::MatrixXd& qp_constraint, const int& horizon);

  bool setStatus(const Eigen::MatrixXd& x, const Eigen::MatrixXd& x_des);

  // get M and C
  void getMCMatrix(const Eigen::MatrixXd& A, const Eigen::MatrixXd& B);

  // qpOASES
  void qpSolver(const Eigen::MatrixXd& x_in, const Eigen::MatrixXd& xs_des);

  // get U
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> getControl();

 private:
  bool isSymmetric(DMat<double> m) {
    for (int i = 0; i < m.rows() - 1; ++i) {
      for (int j = i + 1; j < m.cols(); ++j) {
        if (m(i, j - i) != m(j - i, i)) return false;
      }
    }
    return true;
  }

  bool first_solver_qp_{true};
  int horizon_;
  Eigen::VectorXd u_;
  Eigen::MatrixXd M_, C_, Q_, R_, H_, G_, qp_A_;
  Eigen::VectorXd qp_lb_, qp_ub_;

  // qpOASES init
  std::unique_ptr<qpOASES::SQProblem> qp_solver_;
  qpOASES::real_t* qp_soln_;
};